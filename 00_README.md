# General information

Title of Dataset: Assessing the Readiness for and Knowledge of BIBFRAME in Canadian Libraries

This README file was written on 2022-09-13 (YYYY-MM-DD) by Daniel B. Scott.  
Last updated: 2022-09-13.

## Description of dataset

This dataset contains the data and survey materials for a study of the
readiness for, and knowledge of, BIBFRAME in Canadian libraries. The
study used a stratified random sample of 1,500 academic, public, school,
and special libraries drawn from a list of 5,812 Canadian libraries. 287
responses were received, representing a 19% response rate.

## Contact information

Name: Heather Pretty  
Institution: Memorial University of Newfoundland, St. John’s, Canada  
Email: <jpretty@mun.ca>  
ORCID: <https://orcid.org/0000-0003-3800-1482>

## Data & file overview

### Folder and file list

The dataset consists of one folder and the following files:

* 00_README.md (plain text file)

    This file (= the current document) documents the dataset.

* anonymized_recoded_data.tsv (tab-separated plain text file, encoded in Unicode UTF-8)

    This file contains the responses from the English and French surveys.

* Data_management_plan.pdf

    This file documents the plans for managing the data, documentation, and
	communications for the research project.
	
* distribution_by_province.csv (comma-separated plain text file)

    This file contains the distribution of responses by Canadian province.

#### Survey materials (folder)

This folder contains two sub-folders with the survey materials in English and French.

##### English

* Survey_invitation.docx (Microsoft Word document)
* Survey_invitation.pdf

    These files are identical except for their file formats. They contain the
	English text of the email that was sent to potential respondents.
	
* Survey_questions.docx (Microsoft Word document)
* Survey_questions.pdf
* Survey_questions.qsf (Qualtrics Survey File)

    These files are identical except for their file formats. They contain the
	survey questions in English.
	
##### French

* Enquête_invitation.docx (Microsoft Word document)
* Enquête_invitation.pdf

    These files are identical except for their file formats. They contain the
	French text of the email that was sent to potential respondents.

* Enquête_questions.docx (Microsoft Word document)
* Enquête_questions.pdf
* Enquête_questions.qsf (Qualtrics Survey File)

    These files are identical except for their file formats. They contain
	the survey questions in French.

## DATA-SPECIFIC INFORMATION FOR: anonymized_recoded_data.tsv

To combine the data from the separate English and French surveys, clean,
anonymize, and analyze the data, Daniel B. Scott wrote a custom Python
application: [analyze_results.py](https://gitlab.com/denials/cbfrwg-survey-analysis).

To verify the results, Alexandre Fortier independently implemented the
recoding and analysis of the data in SPSS.

### Variable/Column List

* Q2: What type of Library do you work in?
* Q3: How many librarians are employed in your library?
* Q4: How many staff are employed in your library?
* Q5: Is your library part of a consortium?
* Q7: What is the language of your catalogue? (Please select all that apply.) 
* Q7_English: Q7 recoded. TRUE if the response included English, FALSE if the response did not include English
* Q7_French: Q7 recoded. TRUE if the response included French, FALSE if the response did not include French
* Q7_Cree: Q7 recoded. TRUE if the response included Cree, FALSE if the response did not include Cree
* Q7_Inuktitut: Q7 recoded. TRUE if the response included Inuktitut, FALSE if the response did not include Inuktitut
* Q7_Inuinnaqtun: Q7 recoded. TRUE if the response included Inuinnaqtun, FALSE if the response did not include Inuinnaqtun
* Q7_Japanese: Q7 recoded. TRUE if the response included Japanese, FALSE if the response did not include Japanese
* Q7_Mandarin: Q7 recoded. TRUE if the response included Mandarin, FALSE if the response did not include Mandarin
* Q7_Arabic: Q7 recoded. TRUE if the response included Arabic, FALSE if the response did not include Arabic
* Q7_Farsi: Q7 recoded. TRUE if the response included Farsi, FALSE if the response did not include Farsi
* Q7_Vietnamese: Q7 recoded. TRUE if the response included Vietnamese, FALSE if the response did not include Vietnamese
* Q7_Spanish: Q7 recoded. TRUE if the response included Spanish, FALSE if the response did not include Spanish
* Q8: What term best describes your primary responsibility or role within your library? 
* Q8 Cataloguing: Q8 recoded. TRUE if the response included Cataloguing, FALSE if the response did not include Cataloguing
* Q9: Does your role include responsibility for training others?
* Q10: Do others in the library report to you? 
* Q11: What catalogue system/vendor does your library currently use?
* Q11_VENDOR: Q11 recoded to consistently identify the catalogue vendor.
* Q12: If your library currently uses MARC records, where do you get them from? (Please select all that apply.)
* Q12_Copy: Q12 recoded. TRUE if the library uses copy cataloguing to get MARC records, FALSE if not.
* Q12_Original: Q12 recoded. TRUE if the library creates MARC records through original cataloguing, FALSE if not. 
* Q12_Unsure: Q12 recoded. TRUE if the respondent is unsure about where their library gets their MARC records, FALSE if not.
* Q12_Vendor: Q12 recoded. TRUE if the library gets their MARC records from a vendor, FALSE if not.
* Q12_OCLC: Q12 recoded. TRUE if the library gets their MARC records from OCLC, FALSE if not.
* Q12_Non-MARC: Q12 recoded. TRUE if library does not use MARC records, FALSE if they do use MARC records.
* Q12_Sky River: Q12 recoded. TRUE if the library gets their MARC records from Sky River, FALSE if not.
* Q13: Does your library report its holdings to OCLC?
* Q14: When does your library plan to transition from MARC to BIBFRAME format? 
* Q15: Before receiving this survey, were you aware of the development of BIBFRAME as a replacement for the MARC bibliographic format? 
* Q16_1: For questions Q16 through Q18, see the instrument in Shea et al's "Organizational readiness for implementing change: a psychometric assessment of a new measure" https://doi.org/10.1186/1748-5908-9-7
* Q16_2: 
* Q16_3: 
* Q16_4: 
* Q16_5: 
* Q16_commitment: ORIC commitment total
* Q17_1: 
* Q17_2: 
* Q17_3: 
* Q17_4: 
* Q17_5: 
* Q17_6: 
* Q17_efficacy: ORIC efficacy total
* Q18_1: 
* Q18_2: 
* Q18_3: 
* Q18_4: 
* Q18_5: 
* Q18_6: 
* Q18_7: 
* Q18_8: 
* Q18_9: 
* Q18_valence: ORIC valence total
* Q19: True or false: every RDF triple consists of exactly one subject, one predicate, and one object. 
* Q20_1: Respondent's confidence in their response to Q19, from no confidence (1) to full confidence (5)
* Q21: True or false: each of the subjects, predicates, and objects of an RDF triple must be associated with a uniquely identifying link, called a Uniform Resource Identifier (URI).
* Q22_1: Respondent's confidence in their response to Q21, from no confidence (1) to full confidence (5)
* Q23:  Select each valid RDF serialization in the following list	
* Q23 JSON-LD: Q23 recoded. If the respondent included this response, TRUE.
* Q23 N-Triples: Q23 recoded. If the respondent included this response, TRUE.
* Q23 RDA:  Q23 recoded. If the respondent included this response, TRUE.
* Q23 RDF/XML: Q23 recoded. If the respondent included this response, TRUE.
* Q23 Turtle: Q23 recoded. If the respondent included this response, TRUE.
* Q23_correct: Q23 recoded. If the respondent selected all of the correct responses, TRUE. If not, FALSE.
* Q24_1: Respondent's confidence in their response to Q23, from no confidence (1) to full confidence (5)
* Q27: To represent a resource in your library collection, what are the base classes that must appear in a description using the BIBFRAME vocabulary?
* Q27_correct: Q27 recoded. If the response was "Work, Instance, and Item", Yes. If not, No.
* Q28_1: Respondent's confidence in their response to Q27, from no confidence (1) to full confidence (5)
* Q29: What set of BIBFRAME statements properly represents the Dewey Decimal call number of a book in your library collection? 
* Q29_correct: Q29 recoded. If the response was correct, Yes. If not, No.
* Q30_1: Respondent's confidence in their response to Q29, from no confidence (1) to full confidence (5)
* Q33: When the subject heading is identified by a link, what is the best way to express a subject heading for a book using the BIBFRAME vocabulary?
* Q33_correct: Q33 recoded. If the response was correct, Yes. If not, No.
* Q34_1: Respondent's confidence in their response to Q33, from no confidence (1) to full confidence (5)
* Q35: Select each of the vocabularies that can be used with BIBFRAME:
* Q35_correct: Q35 recoded. If the respondent selected all of the vocabularies (BIBO, DCMI, Schema.org, RDA, and FaBiO), Yes. If not, No.
* Q36_1: Respondent's confidence in their response to Q35, from no confidence (1) to full confidence (5)
* Q39: Select all of the sources of links that BIBFRAME allows for use as identifiers in resource descriptions:
* Q39_correct: Q39 recoded. If the response was correct, Yes. If not, No.
* Q39_correct_strict: Q39 recoded. If the only response was "Any web domain", Yes. If not, No.
* Q40_1: Respondent's confidence in their response to Q39, from no confidence (1) to full confidence (5)
* Q41: What kind of interface will most cataloguers use to create a resource description in BIBFRAME?
* Q41_correct: Q41 recoded. If the response was correct, Yes. If not, No.
* Q42_1: Respondent's confidence in their response to Q41, from no confidence (1) to full confidence (5)
* Q43: How will most people find library resources that have been described with BIBFRAME? 
* Q43_correct: Q42 recoded. If the response was correct, Yes. If not, No.
* Q44_1: Respondent's confidence in their response to Q44, from no confidence (1) to full confidence (5)

Responses to some of the questions, such as Q29, identify an image.

## DATA-SPECIFIC INFORMATION FOR: distribution_by_province.csv

### Variable/Column List

1. Province name, or total
2. Number of responses
3. Percentage of responses
