#!/usr/bin/env python
"""
Analyzes the results of the Canadian BIBFRAME Readiness survey
"""

import pandas as pd
import scipy.stats as scs
import numpy as np
import researchpy as rp
import re
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm


def read_tsv(infile):
    "Reads the given file, returns a Dataframe"
    with open(infile, "r", encoding="utf-16") as src:
        df = pd.read_table(src)

    # First three rows are headers
    df.drop([0, 1], inplace=True)
    return df


def cramers_corrected_stat(confusion_matrix):
    """calculate Cramers V statistic for categorial-categorial association.
    uses correction from Bergsma and Wicher,
    Journal of the Korean Statistical Society 42 (2013): 323-328
    from https://stackoverflow.com/a/39266194
    """
    chi2 = scs.chi2_contingency(confusion_matrix)[0]
    n = confusion_matrix.sum().sum()
    phi2 = chi2 / n
    r, k = confusion_matrix.shape
    phi2corr = max(0, phi2 - ((k - 1) * (r - 1)) / (n - 1))
    rcorr = r - ((r - 1) ** 2) / (n - 1)
    kcorr = k - ((k - 1) ** 2) / (n - 1)

    min_r_k = min((kcorr - 1), (rcorr - 1))

    if min_r_k == 0:
        print("\nr={}, k={}, rcorr={}, kcorr={}".format(r, k, rcorr, kcorr))
        return (
            np.sqrt(phi2),
            np.sqrt(phi2corr),
            0,
        )

    return (
        np.sqrt(phi2),
        np.sqrt(phi2corr),
        np.sqrt(phi2corr / min_r_k),
    )


def write_results(combined, cols, outfile):
    "Write results to a TSV file"
    with (open(outfile, "w")) as of:
        combined.to_csv(
            of, sep="\t", index=False, columns=[x for x in cols.keys() if x != "Q6"]
        )


def clean_responses(combined):
    "Remove pilot, incomplete, and no consent given responses"

    # Drop pilot responses
    combined = combined[lambda df: df.Status == "IP Address"]

    print("Total number of responses: {}\n".format(len(combined.index)))

    # Count responses that were not complete
    incomplete = combined[lambda df: df.Finished != "True"]
    print("Number of incomplete responses: {}\n".format(len(incomplete.index)))

    # Drop any responses that were not complete
    combined = combined[lambda df: df.Finished == "True"]

    # Count responses that did not consent
    nonconsensual = combined[
        lambda df: (df.Q1 == "I do not consent") | (df.Q1 == "Je ne consent pas")
    ]
    print(
        "Number of respondents that did not consent: {}\n".format(
            len(nonconsensual.index)
        )
    )

    # Drop any responses that did not consent
    combined = combined[
        lambda df: (df.Q1 == "I consent, begin the survey in English")
        | (df.Q1 == "Je consens, commencer le sondage en français")
    ]

    return combined


def clean_columns(combined):
    "Drop potentially identifying or useless columns"

    # Drop identifying info
    combined = combined.drop(
        ["IPAddress", "LocationLatitude", "LocationLongitude"], axis="columns"
    )

    # Drop series we won't analyze
    combined = combined.drop(
        [
            "StartDate",
            "EndDate",
            "Status",
            "Progress",
            "Finished",
            "Duration (in seconds)",
            "RecipientLastName",
            "RecipientFirstName",
            "RecipientEmail",
            "ExternalReference",
            "DistributionChannel",
        ],
        axis="columns",
    )

    return combined


def anonymize(combined):
    "Drop identifying columns after analysis"
    combined = combined.drop(
        [
            "Q2_7_TEXT",
            "Q2_5_TEXT",
            "Q2_OTHER",
            "Q6",
            "Q7_4_TEXT",
            "Q11_10_TEXT",
            "Q11_15_TEXT",
            "Q11_16_TEXT",
        ],
        axis="columns",
    )

    return combined


def recode_values(combined):
    "Make responses consistent"

    # Replace French phrases with English equivalents
    combined.replace(
        {
            "Autre": "Other",
            "Oui": "Yes",
            "Non": "No",
            "Incertain(e)": "Unsure",
            "Je ne sais pas": "I don't know",
            "Aucune certitude:  1": "No confidence:  1",
            "Certitude totale: 5": "Full confidence: 5",
            "Vrai": "True",
            "Désaccord complet avec l'affirmation 0": "Disagree completely 0",
            "Accord complet  avec l'affirmation 10": "Agree completely 10",
            "Scolaire": "School",
            "Publique ou municipale": "Public",
            "Spécialisée (par exemple, les bibliothèques au service des sociétés, des hôpitaux, des organismes militaires, des musées, des entreprises privées ou du gouvernement)": "Special",
            "Universitaire ou collégiale": "Academic",
            "Nouveau-Brunswick": "New Brunswick",
            "Quebec": "Québec",
            "Anglais": "English",
            "Français": "French",
            "Anglais,Français": "English,French",
            "Catalogage et métadonnées": "Cataloguing & Metadata",
            "Prêts": "Circulation",
            "Initiatives numériques": "Digital Initiatives",
            "Services techniques": "Technical Services",
            "Systèmes": "Systems",
            "Développement de collections": "Collections",
            "Services d'information": "Information Services",
            "Catalogage dérivé à partir de serveurs Z39.50": "Copy cataloguing from Z39.50 servers",
            "Catalogage dérivé à partir de serveurs Z39.50,Autre": "Copy cataloguing from Z39.50 servers,Other",
            "Catalogage dérivé à partir de serveurs Z39.50,Catalogage original": "Copy cataloguing from Z39.50 servers,Original cataloguing",
            "Catalogage dérivé à partir de serveurs Z39.50,Catalogage original,Autre": "Copy cataloguing from Z39.50 servers,Original cataloguing,Other",
            "Fournisseur,Autre": "Vendor,Other",
            "Fournisseur,Catalogage dérivé à partir de serveurs Z39.50,Catalogage original": "Vendor,Copy cataloguing from Z39.50 servers,Original cataloguing",
            "Fournisseur,Catalogage dérivé à partir de serveurs Z39.50,Catalogage original,Autre": "Vendor,Copy cataloguing from Z39.50 servers,Original cataloguing,Other",
            "Catalogage original": "Original cataloguing",
            "Catalogage original,Autre": "Original cataloguing,Other",
            "OCLC,Catalogage dérivé à partir de serveurs Z39.50": "OCLC,Copy cataloguing from Z39.50 servers",
            "OCLC,Catalogage dérivé à partir de serveurs Z39.50,Autre": "OCLC,Copy cataloguing from Z39.50 servers,Other",
            "OCLC,Catalogage dérivé à partir de serveurs Z39.50,Catalogage original": "OCLC,Copy cataloguing from Z39.50 servers,Original cataloguing",
            "OCLC,Fournisseur,Catalogage dérivé à partir de serveurs Z39.50,Catalogage original": "OCLC,Vendor,Copy cataloguing from Z39.50 servers,Original cataloguing",
            "Ma bibliothèque n'utilise pas présentement de notices MARC": "My library does not currently use MARC records",
            "Déjà amorcée": "Already underway",
            "D'ici 1 à 2 ans": "Within 1-2 years",
            "D'ici 5 à 10 ans": "Within 5-10 years",
            "Nous n'en savons pas encore suffisamment sur BIBFRAME pour envisager une transition": "We do not yet know enough about BIBFRAME to consider planning a transition",
            "Nous ne prévoyons pas passer du format MARC à BIBFRAME au cours des 10 prochaines années": "We do not plan to transition from MARC to BIBFRAME within the next 10 years",
            "Nous prévoyons continuer à utiliser les notices MARC et ne passerons pas à BIBFRAME": "We plan to continue using MARC records and will not transition to BIBFRAME",
            "Ensemble des tâches car 1 seul employé": "All of the above",
            "Tous ces choix": "All of the above",
            "Tout": "All of the above",
            "Tout cela": "All of the above",
            "Tout ça": "All of the above",
            "Toutes ces réponses+aménagements": "All of the above",
            "Toutes ces réponses": "All of the above",
            "Un peu de tout cela!": "All of the above",
            "l'ensemble de ces réponses": "All of the above",
            "L'ensemble de ces réponses": "All of the above",
            "tous": "All of the above",
            "technicienne responsable donc toutes les tâches": "All of the above",
            "toutes ces responsabilités": "All of the above",
            "toutes ces réponses": "All of the above",
            "bilingue": "Bilingual",
            "Bilingue": "Bilingual",
            "Faux": "False",
            "Œuvre, instance et item": "Work, Instance, and Item",
            "Œuvre, expression, manifestation et item": "Work, Expression, Manifestation, and Item",
            "Œuvre, emplacement et instance": "Work, Location, and Item",
            "Les catalogueurs choisiront un formulaire adapté au type de ressources qu'ils veulent décrire (par exemple, « Blu-Ray » ou « livre »), lequel fournira la plupart des informations techniques requises; par la suite, ils utiliseront des entrées par requêtes prédictives afin de trouver des liens vers les noms et les vedettes-matières appropriées. Ils ne saisiront manuellement qu'un nombre limité d'éléments d'information tels que les codes à barres ou les notes de contenu.": "Cataloguers will select a template for the kind of resource they want to describe (for example, “Blu-ray” or “Book”) that supplies most of the technical information, use typeahead search to find links for name and subject authorities, and only manually enter limited information such as barcodes or content notes.",
            "Les catalogueurs utiliseront un éditeur MARC standard pour créer une description en utilisant le format MARC, puis le système convertira les données MARC au format BIBFRAME.": "Cataloguers will use a standard MARC editor to create a description using the MARC format, then the system will convert the MARC data to BIBFRAME.",
            "Les catalogueurs utiliseront un éditeur de texte pour entrer les triplets RDF dans le format de sérialisation de leur choix.": "Cataloguers will use a text editor to enter RDF triples in their preferred serialization format.",
            "Les usagers lanceront une requête à l'aide d'un lien qui identifie la ressource de bibliothèque. En cliquant sur ce lien, un graphe de réseau s'ouvrira, dans lequel chaque nœud représentera un lien associé à la description de ressource BIBFRAME. En cliquant sur un nœud, tous les liens associés à celui-ci s'afficheront; de plus, positionner le curseur au-dessus d'un nœud permettra de visualiser le texte associé à un lien particulier. Le système de bibliothèques regroupera les ressources les plus pertinentes au centre du graphe, tandis que les ressources moins pertinentes seront dispersées en bordure du graphe.": "Users will search for a link that identifies the library resource. Clicking on the link will open a network graph, in which every node represents a link associated with the BIBFRAME resource description. Clicking on a node will reveal all of its associated links, while hovering over a node will display the text associated with a link. The library system will display the most relevant resources clustered closely together in the centre of the graph, with less relevant resources at the edges.",
            "Les usagers rechercheront un terme ou une expression dans le système de bibliothèques. Le système de bibliothèques recherchera des correspondances dans un index de recherche contenant les étiquettes pour chacun des liens associés à une description de ressource BIBFRAME. Le système de bibliothèques affichera les ressources correspondantes triées par pertinence dans une liste contenant leur titre, leur auteur, la date de création, etc.": "In the library system, users will search for a term or phrase. The library system will look for matches in a search index which contains the labels associated with each link related to a BIBFRAME resource description. The library system will display matching resources sorted by relevance in a list with their title, author, creation date, etc.",
            "Bibliothèque du Congrès (id.loc.gov),Bibliothèque et Archives Canada (lac-bac.gc.ca),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org)": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Virtual International Authority File (viaf.org)",
            "Bibliothèque du Congrès (id.loc.gov),Bibliothèque et Archives Canada (lac-bac.gc.ca)": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov)",
            "Bibliothèque du Congrès (id.loc.gov),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org)": "Library of Congress (id.loc.gov),Virtual International Authority File (viaf.org)",
            "Bibliothèque du Congrès (id.loc.gov),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org),Wikidata (wikidata.org)": "Library of Congress (id.loc.gov),Virtual International Authority File (viaf.org),Wikidata (wikidata.org)",
            "Bibliothèque du Congrès (id.loc.gov)": "Library of Congress (id.loc.gov)",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca),Bibliothèque du Congrès (id.loc.gov),Online Computer Library Center (oclc.org),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org),Wikidata (wikidata.org)": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Online Computer Library Center (oclc.org),Virtual International Authority File (viaf.org),Wikidata (wikidata.org)",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca),Bibliothèque du Congrès (id.loc.gov),Online Computer Library Center (oclc.org),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org),Wikidata (wikidata.org),N'importe quel domaine Internet": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Online Computer Library Center (oclc.org),Virtual International Authority File (viaf.org),Wikidata (wikidata.org),Any web domain",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca),Bibliothèque du Congrès (id.loc.gov),Online Computer Library Center (oclc.org),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org),N'importe quel domaine Internet": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Online Computer Library Center (oclc.org),Virtual International Authority File (viaf.org),Any web domain",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca),Bibliothèque du Congrès (id.loc.gov),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org)": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Virtual International Authority File (viaf.org)",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca),Bibliothèque du Congrès (id.loc.gov),Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org),Wikidata (wikidata.org)": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Virtual International Authority File (viaf.org),Wikidata (wikidata.org)",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca),Bibliothèque du Congrès (id.loc.gov),Online Computer Library Center (oclc.org)": "Library & Archives Canada (lac-bac.gc.ca),Library of Congress (id.loc.gov),Online Computer Library Center (oclc.org)",
            "Bibliothèque et Archives Canada (lac-bac.gc.ca)": "Library & Archives Canada (lac-bac.gc.ca)",
            "Fichier d'autorité international virtuel (en anglais, Virtual International Authority File, viaf.org)": "Virtual International Authority File (viaf.org)",
            "N'importe quel domaine Internet": "Any web domain",
        },
        value=None,
        inplace=True,
    )

    # Fix some English inconsistencies
    combined.replace(
        {
            "I don’t know": "I don't know",
            "Disgree completely 0": "Disagree completely 0",
            "All": "All of the above",
            "All above": "All of the above",
            "All of the Above": "All of the above",
            "All professional librarian functions": "All of the above",
            "Everything": "All of the above",
            "Everything ": "All of the above",
            "Manager/Sole Librarian - All of the Above": "All of the above",
            "School library so all of the above": "All of the above",
            "Solo librarian - most of the above": "All of the above",
            "Solo librarian -- so all of it": "All of the above",
            "all of the above": "All of the above",
            "everything - Solo": "All of the above",
            "bilingual": "Bilingual",
            "Bilingual En-Fr": "Bilingual",
            "Special (for example, libraries serving corporations, hospitals, the military, museums, private businesses, or the government)": "Special",
        },
        value=None,
        inplace=True,
    )

    # Make Likert scales just plain integers
    combined.replace(
        {
            "Disagree completely 0": 0,
            "Agree completely 10": 10,
            "Full confidence: 5": 5,
            "No confidence:  1": 1,
        },
        value=None,
        inplace=True,
    )

    combined = combined.astype(
        {
            "Q16_1": "float",
            "Q16_2": "float",
            "Q16_3": "float",
            "Q16_4": "float",
            "Q16_5": "float",
            "Q17_1": "float",
            "Q17_2": "float",
            "Q17_3": "float",
            "Q17_4": "float",
            "Q17_5": "float",
            "Q17_6": "float",
            "Q18_1": "float",
            "Q18_2": "float",
            "Q18_3": "float",
            "Q18_4": "float",
            "Q18_5": "float",
            "Q18_6": "float",
            "Q18_7": "float",
            "Q18_8": "float",
            "Q18_9": "float",
            "Q20_1": "float",
            "Q22_1": "float",
            "Q24_1": "float",
            "Q28_1": "float",
            "Q30_1": "float",
            "Q34_1": "float",
            "Q36_1": "float",
            "Q40_1": "float",
            "Q42_1": "float",
            "Q44_1": "float",
        }
    )

    # Split out catalogue language responses into separate variables
    combined = combined.assign(
        Q7_English=lambda df: (
            df["Q7"].str.contains("English") | (df["Q7_4_TEXT"] == "Bilingual")
        )
        | False
    )
    combined = combined.assign(
        Q7_French=lambda df: (
            df["Q7"].str.contains("French") | (df["Q7_4_TEXT"] == "Bilingual")
        )
        | False
    )

    combined = combined.assign(Q7_Cree=lambda df: df["Q7_4_TEXT"] == "cree")
    combined = combined.assign(
        Q7_Inuktitut=lambda df: (df["Q7_4_TEXT"].str.contains("Inuktitut")) | False
    )
    combined = combined.assign(
        Q7_Inuinnaqtun=lambda df: (df["Q7_4_TEXT"].str.contains("Inuinnaqtun")) | False
    )
    combined = combined.assign(
        Q7_Japanese=lambda df: (df["Q7_4_TEXT"].str.contains("Japanese")) | False
    )
    combined = combined.assign(
        Q7_Mandarin=lambda df: (df["Q7_4_TEXT"].str.contains("Mandarin")) | False
    )
    combined = combined.assign(
        Q7_Arabic=lambda df: (df["Q7_4_TEXT"].str.contains("Arabic")) | False
    )
    combined = combined.assign(
        Q7_Farsi=lambda df: (df["Q7_4_TEXT"].str.contains("Farsi")) | False
    )
    combined = combined.assign(
        Q7_Vietnamese=lambda df: (df["Q7_4_TEXT"].str.contains("Vietnamese")) | False
    )
    combined = combined.assign(
        Q7_Spanish=lambda df: (df["Q7_4_TEXT"].str.contains("Spanish")) | False
    )

    # Merge English and French "Other" responses
    combined["Q2_OTHER"] = combined["Q2_5_TEXT"].str.cat(
        combined[
            [
                "Q2_7_TEXT",
            ]
        ],
        sep="",
        na_rep="",
    )

    return combined.copy()


def recode_role_other(df):
    """
    Recode Q8 role type: Other responses
    """

    for txt in (
        "All of the above",
        "All of the above, as the sole librarian in a special library",
        "Director/Solo Librarian",
        "I have responsibilities in all of the above categories due to our small staff complement.",
        "Manager/Sole Librarian - All of the Above ",
        "small library where we do a little bit of everything",
        "Toutes ses réponses",
    ):
        df.loc[
            (df["Q8"] == "Other") & (df["Q8_10_TEXT"] == txt), "Q8"
        ] = "All of the above"
        df.loc[(df["Q8_10_TEXT"] == txt), "Q8_10_TEXT"] = np.nan

    for txt in (
        "CEO",
        "CEO/Branch Librarian",
        "CEO, but do the cataloguing and most other things",
        "Gestionnaire",
        "Head public librarian ",
        "Head public librarian",
        "Library Manager",
        "library manager",
        "Manager/Librarian",
        "responsable du centre de documentation",
        "supervision générale",
    ):
        df.loc[
            (df["Q8"] == "Other") & (df["Q8_10_TEXT"] == txt), "Q8"
        ] = "Administration"
        df.loc[(df["Q8_10_TEXT"] == txt), "Q8_10_TEXT"] = np.nan

    for txt in (
        "Recherche documentaire et veilles",
        "Recherche et services aux usagers",
        "référence",
        "Research",
        "veille documentaire",
    ):
        df.loc[
            (df["Q8"] == "Other") & (df["Q8_10_TEXT"] == txt), "Q8"
        ] = "Information Services"
        df.loc[(df["Q8_10_TEXT"] == txt), "Q8_10_TEXT"] = np.nan

    for txt in (
        "Recherche documentaire et veilles",
        "Services tecniques (incluant systèmes, acquisitions, développement de collections et catalogage)",
        "Technology & Technical Services",
    ):
        df.loc[
            (df["Q8"] == "Other") & (df["Q8_10_TEXT"] == txt), "Q8"
        ] = "Technical Services"
        df.loc[(df["Q8_10_TEXT"] == txt), "Q8_10_TEXT"] = np.nan

    # Initialize new column
    df.loc[(df["Q8"].any() & df["Q8"].str.match(r".")), "Q8 Cataloguing"] = False
    for txt in ("All of the above", "Cataloguing & Metadata"):
        df.loc[(df["Q8"] == txt), "Q8 Cataloguing"] = True
        df.loc[
            (df["Q8"] == "Other") & (df["Q8_10_TEXT"].str.contains("Catalog")),
            "Q8 Cataloguing",
        ] = True


def recode_system_type_other(df):
    """
    Recode Q11 Library system type: Other responses

    We have three columns for Other responses:
    * Q11_10_TEXT - vendor is OCLC
    * Q11_15_TEXT - vendor is SirsiDynix
    * Q11_16_TEXT - vendor is Other

    Goal is to recode "Other" in Q11 as the system values, Q11_VENDOR as the
    vendor values
    """

    df["Q11_VENDOR"] = np.nan
    df.loc[
        (df["Q11"] == "OCLC - Other") & (df["Q11_10_TEXT"] == "VDX"), "Q11_VENDOR"
    ] = "OCLC"
    df.loc[
        (df["Q11"] == "OCLC - Other") & (df["Q11_10_TEXT"] == "VDX"), "Q11"
    ] = "OCLC - VDX"

    sd = "SirsiDynix"
    for k, v in {
        "EOS": "EOS.Web",
        "JASI": "Symphony",
        "Work Flows": "Symphony",
        "Workflows": "Symphony",
    }.items():
        df.loc[
            (df["Q11"] == "{} - Other".format(sd)) & (df["Q11_15_TEXT"] == k),
            "Q11_VENDOR",
        ] = sd
        df.loc[
            (df["Q11"] == "{} - Other".format(sd)) & (df["Q11_15_TEXT"] == k),
            "Q11_15_TEXT",
        ] = v
    for k in df["Q11_15_TEXT"]:
        df.loc[
            (df["Q11"] == "{} - Other".format(sd)) & (df["Q11_15_TEXT"] == k), "Q11"
        ] = "{} - {}".format(sd, k)

    others = (
        ("Access Database", "Access database", "Microsoft"),
        ("Alexandria", "Alexandria", "Alexandria"),
        ("All of the above", "Other", "Other"),
        ("Biblio mondo - Portfolio", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("BiblioMondo", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("BIblioMondo - Portfolio", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("Bibliomondo (Portfolio / InMedia)", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("BiblioMondo PortFolio", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("Biblionet", "Biblionet", "Concepts logiques 4DI inc."),
        ("Biblionix - Apollo", "Biblionix - Apollo", "Biblionix"),
        ("Coba", "Coba", "Coba"),
        ("DB Text", "Inmagic DB/TextWorks", "Lucidea"),
        ("DB/Textworks", "Inmagic DB/TextWorks", "Lucidea"),
        ("DBText", "Inmagic DB/TextWorks", "Lucidea"),
        ("DBTextworks", "Inmagic DB/TextWorks", "Lucidea"),
        ("DBTextWorks", "Inmagic DB/TextWorks", "Lucidea"),
        ("Destiny", "Follett Destiny Library Manager", "Follett"),
        ("Eloquent", "Eloquent Systems", "Lucidea"),
        ("EndNote", "EndNote", "Clarivate Analytics"),
        ("EOS", "SirsiDynix - EOS.Web", "SirsiDynix"),
        ("FGB", "Plurilogic - FGB", "Plurilogic"),
        ("Follet Destiny", "Follett Destiny Library Manager", "Follett"),
        ("Follett", "Follett Destiny Library Manager", "Follett"),
        ("follett destiny", "Follett Destiny Library Manager", "Follett"),
        ("Follett Destiny", "Follett Destiny Library Manager", "Follett"),
        ("foxtrot", "Other", "Other"),
        ("Genie", "Lucidea - GeniePlus", "Lucidea"),
        ("Genie PLUS", "Lucidea - GeniePlus", "Lucidea"),
        ("GRICS - Regard", "GRICS - Regard", "GRICS"),
        ("Home-built system", "Other", "Other"),
        ("In Media Technologies", "BiblioMondo - InMedia", "BiblioMondo"),
        ("Infor", "V-smart", "Infor"),
        ("Infor - V-Smart", "V-smart", "Infor"),
        ("Infor Vsmart", "V-smart", "Infor"),
        ("Inmagic DB TEXTWorks", "Inmagic DB/TextWorks", "Lucidea"),
        ("Inmagic DB/Textworks", "Inmagic DB/TextWorks", "Lucidea"),
        ("Inmagic DB/TextWorks", "Inmagic DB/TextWorks", "Lucidea"),
        ("Inmagic DB/TextWorks and Quickbase", "Inmagic DB/TextWorks", "Lucidea"),
        ("InMagic DBText", "Inmagic DB/TextWorks", "Lucidea"),
        ("Insignia", "Insignia Library System", "Insignia Software"),
        ("Insignia Library Software", "Insignia Library System", "Insignia Software"),
        ("jasi", "SirsiDynix - Symphony", "SirsiDynix"),
        ("Kentika", "Kentika", "Kentika"),
        ("Kentika Atomic", "Kentika Portail Atomic", "Kentika"),
        ("L4U", "L4U", "L4U Library Software"),
        ("L4U by PowerSchool", "L4U", "L4U Library Software"),
        ("Liberty", "softlink - Liberty", "softlink"),
        ("Lotus Notes", "IBM - Lotus Notes", "IBM"),
        ("Lucidea - Genie+", "Lucidea - GeniePlus", "Lucidea"),
        ("Lucidea - SydneyPlus", "Sydney", "Lucidea"),
        ("Lucidea DB/Textworks", "Inmagic DB/TextWorks", "Lucidea"),
        ("Lucidea GeniePlus", "Lucidea - GeniePlus", "Lucidea"),
        ("Lucidea/SydneyPlus GeniePlus", "Lucidea - Sydney", "Lucidea"),
        ("Mandarin M5", "Mandarin - M5", "Mandarin"),
        ("MondoIn", "BiblioMondo - InMedia", "BiblioMondo"),
        ("not sure", "Other", "Other"),
        ("One developed in-house", "In-house", "Other"),
        ("OPALS", "Media Flex - OPALS", "Media Flex Inc."),
        ("Polaris", "Innovative Interfaces - Polaris", "Innovative Interfaces"),
        ("Polaris/Leap", "Innovative Interfaces - Polaris", "Innovative Interfaces"),
        ("Porfolio", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("Portfolio", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("PortFolio", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("Portfolio - InMedia", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("Portfolio de Bibliomondo", "BiblioMondo - Portfolio", "BiblioMondo"),
        ("Portfolio/In Média Technologies", "BiblioMondo - InMedia", "BiblioMondo"),
        ("Regard", "GRICS - Regard", "GRICS"),
        ("REGARD", "GRICS - Regard", "GRICS"),
        ("Regard (la plupart) et Koha (1 école)", "GRICS - Regard", "GRICS"),
        ("Regard 9.20 fourni par GRICS", "GRICS - Regard", "GRICS"),
        ("Regard/GRICS", "GRICS - Regard", "GRICS"),
        ("ResourceMate", "Jaywil - ResourceMate", "Jaywil Software Development"),
        ("SirsiDynix - Eos", "SirsiDynix - EOS.Web", "SirsiDynix"),
        ("SirsiDynix Symphony", "SirsiDynix - Symphony", "SirsiDynix"),
        ("Soutron", "Soutron Library Management System", "Soutron"),
        ("Surpass", "Surpass", "Surpass Software"),
        ("Sydney", "Lucidea - Sydney", "Lucidea"),
        ("Sydney Enterprise", "Lucidea - Sydney", "Lucidea"),
        ("système maison", "In-house", "Other"),
        (
            "Système maison Base de données relationnelles Access",
            "Access database",
            "Microsoft",
        ),
        (
            "The Library Corporation",
            "Library Solution",
            "The Library Corporation (TLC)",
        ),
        (
            "The Library Corporation (i'm pretty sure)",
            "Library Solution",
            "The Library Corporation (TLC)",
        ),
        ("VSmart", "V-smart", "Infor"),
        ("V-smart ( ?)", "V-smart", "Infor"),
        (
            "We are small and mainly use an Excel spreadsheet",
            "Excel spreadsheet",
            "Microsoft",
        ),
    )
    for raw, recode, vendor in others:
        df.loc[
            (df["Q11"] == "Other") & (df["Q11_16_TEXT"] == raw), "Q11_VENDOR"
        ] = "{}".format(vendor)
        df.loc[
            (df["Q11"] == "Other") & (df["Q11_16_TEXT"] == raw), "Q11"
        ] = "{}".format(recode)

    vendors = (
        "Evergreen",
        "Koha",
        "Mandarin",
        "Innovative Interfaces",
        "SirsiDynix",
        "OCLC",
        "Ex Libris",
        "Other",
    )
    for vendor in vendors:
        df.loc[
            (df["Q11"].str.contains(vendor, na=False)) & (df["Q11_VENDOR"].isnull()),
            "Q11_VENDOR",
        ] = "{}".format(vendor)


def recode_library_type_other(df):
    "Recode library type Other responses"

    mask = (df["Q2"] == "Other") & (
        (df["Q2_OTHER"] == "Law Society")
        | (df["Q2_OTHER"] == "courthouse")
        | (df["Q2_OTHER"] == "Public Legal information (ngo)")
        | (df["Q2_OTHER"] == "Law")
        | (df["Q2_OTHER"] == "Société privée")
        | (df["Q2_OTHER"] == "Teacher resource library")
    )
    df["Q2"] = df["Q2"].mask(mask, "Special")
    df["Q2_OTHER"] = df["Q2_OTHER"].mask(mask, None)

    mask = (df["Q2"] == "Other") & (
        (df["Q2_OTHER"] == "School Board") | (df["Q2_OTHER"] == "Conseil scolaire")
    )
    df["Q2"] = df["Q2"].mask(mask, "School")
    df["Q2_OTHER"] = df["Q2_OTHER"].mask(mask, None)

    mask = (df["Q2"] == "Other") & (
        (df["Q2_OTHER"] == "Public")
        | (df["Q2_OTHER"] == "centre régional de services aux bibliothèques publiques")
        | (
            df["Q2_OTHER"]
            == "Réseau BIBLIO de la Capitale-Nationale et de la Chaudières-Appalache"
        )
        | (
            df["Q2_OTHER"]
            == "Government office, representing the public library system (it's centralized in Saskatchewan)"
        )
        | (df["Q2_OTHER"] == "Municipal")
    )
    df["Q2"] = df["Q2"].mask(mask, "Public")
    df["Q2_OTHER"] = df["Q2_OTHER"].mask(mask, None)

    # Leaves just:
    # Double volet : Collégial et municipal
    # Volunteer Library

    # Drop the library vendors
    df.drop(df[df["Q2_OTHER"] == "Library Vendor"].index, inplace=True)
    df.drop(df[df["Q2_OTHER"] == "Library Wholesaler"].index, inplace=True)


def recode_marc_source(df):
    "Create subvariables True/False for each possible source (Q12)"

    sources = (
        ("Copy cataloguing from Z39.50 servers", "Q12_Copy"),
        ("Original cataloguing", "Q12_Original"),
        ("Unsure", "Q12_Unsure"),
        ("Vendor", "Q12_Vendor"),
        ("OCLC", "Q12_OCLC"),
        ("My library does not currently use MARC records", "Q12_Non-MARC"),
        ("Sky River", "Q12_Sky River"),
    )

    for s, code in sources:
        df.loc[(df["Q12"].any()) & (df["Q12"].str.contains(s)), code] = True


def calc_oric(df):
    "Calculates the ORIC scores"

    df["Q16_commitment"] = (
        df["Q16_1"] + df["Q16_2"] + df["Q16_3"] + df["Q16_4"] + df["Q16_5"]
    )
    df["Q17_efficacy"] = (
        df["Q17_1"]
        + df["Q17_2"]
        + df["Q17_3"]
        + df["Q17_4"]
        + df["Q17_5"]
        + df["Q17_6"]
    )
    df["Q18_valence"] = (
        df["Q18_1"]
        + df["Q18_2"]
        + df["Q18_3"]
        + df["Q18_4"]
        + df["Q18_5"]
        + df["Q18_6"]
        + df["Q18_7"]
        + df["Q18_8"]
        + df["Q18_9"]
    )


def create_Q23_subvariables(df):
    "Generate True/False subvariables for every possible answer"

    subvars = (
        ("JSON-LD", "Q23 JSON-LD"),
        ("N-Triples", "Q23 N-Triples"),
        ("RDA", "Q23 RDA"),
        ("RDF/XML", "Q23 RDF/XML"),
        ("Turtle", "Q23 Turtle"),
    )

    for var, code in subvars:
        df.loc[(df["Q23"].any()) & (df["Q23"].str.contains(var)), code] = True
        # initialize all of the responses as False
        df.loc[(df["Q23"].any()) & (df["Q23"].str.contains(var)), "Q23_correct"] = False

    # flip the correct responses to True
    df.loc[
        (df["Q23 JSON-LD"] == True)
        & (df["Q23 N-Triples"] == True)
        & (df["Q23 RDF/XML"] == True)
        & (df["Q23 Turtle"] == True),
        "Q23_correct",
    ] = True

    df.loc[
        (df["Q23"].any()) & (df["Q23"].str.contains("I don't know")), "Q23_correct"
    ] = "I don't know"


def create_correct(df, col, correct):
    "Provide the correct answer, or I don't know"

    correct_col = "{}_correct".format(col)
    df.loc[(df[col].any()) & (df[col].str.match(r".")), correct_col] = "No"
    df.loc[(df[col].any()) & (df[col].str.contains(correct)), correct_col] = "Yes"
    df.loc[
        (df[col].any()) & (df[col].str.contains("I don't know")), correct_col
    ] = "I don't know"


def calc_chi_square(df, questions, col1, col2, alpha=0.05, always_print=False):
    """
    Calculate the χ2 across two columns

    Relies on researchpy, which throws division-by-zero errors for some datasets
    """

    try:
        ct, res, expected = rp.crosstab(
            df[col1],
            df[col2],
            test="chi-square",
            expected_freqs=True,
            correction=True,
            cramer_correction=True,
        )
        print(
            "\n=== {}: {} by {}: {}".format(
                col2, questions[col2], col1, questions[col1]
            )
        )

        chi2 = res.at[0, "results"]
        p = res.at[1, "results"]
        cramers_v = res.at[2, "results"]
        dof = re.sub(
            r"^.*?\(\s*(\d+\.?\d*?)\) = $", r"\1", res.at[0, "Chi-square test"]
        )
        n = ct.iat[-1, -1]
        if p > alpha:
            print("*Not significant*")
        if p <= alpha or always_print is True:
            print_chi_square(ct, expected, col1, col2, dof, n, chi2, p, cramers_v)
    except ZeroDivisionError:
        # Fall back to manual method
        calc_chi_square_old(df, questions, col1, col2, alpha, always_print)


def calc_chi_square_old(df, questions, col1, col2, alpha=0.05, always_print=False):
    "Calculate the chi-square across two columns"

    print("\n=== {}: {} by {}: {}".format(col2, questions[col2], col1, questions[col1]))
    ct = pd.crosstab(index=df[col1], columns=df[col2])
    chi2, p, dof, expected = scs.chi2_contingency(ct)

    # Turn these np.ndarrays into DataFrames
    ct = pd.DataFrame(data=ct)
    expected = pd.DataFrame(data=expected)

    (phi2, phi2corr, cramers_v) = cramers_corrected_stat(ct)
    n = ct.sum(numeric_only=True).sum()
    if p > alpha:
        print("*Not significant*")
    if p <= alpha or always_print is True:
        print_chi_square(ct, expected, col1, col2, dof, n, chi2, p, cramers_v)


def print_chi_square(ct, expected, col1, col2, dof, n, chi2, p, cramers_v):
    "Print neatly formatted χ2 results"

    print(".....")
    print("\nchi-square - {} {}:".format(col2, col1))
    print("\nActual values:")

    if "1-5" in ct.index or "> 50" in ct.index:
        ct.index = sorted(ct.index, key=sort_range)

    print(ct)
    print("\nExpected values:")

    if "1-5" in expected.index or "> 50" in expected.index:
        expected.index = sorted(expected.index, key=sort_range)

    print(expected)
    print(".....")

    # Remove leading 0 from p (thanks APA)
    p0 = repr(f"{p:.3f}")[2:-1]
    res = f"\nχ2 ({dof}, __N__ = {n:.2f}), __p__ = {p0}"

    # Only print V if it is non-zero
    if cramers_v != 0:
        # Remove leading 0 from V (thanks APA)
        v = repr(f"{cramers_v:.2f}")[2:-1]
        res = res + f", __V__ = {v}"
    print(res)


def sort_range(key):
    "Sorts a range in correct order"

    if key == "All":
        return 1000
    return int(re.sub(r"^.*?(\d+)\D*?$", r"\1", key))


def describe_stats(combined, questions):
    "Generate descriptive stats for the results"

    with pd.option_context(
        "display.max_rows", 1000, "display.max_columns", 500, "display.width", 1000
    ):
        for q in questions:
            if q == "ResponseId":
                continue
            print("=== {} - {}".format(q, questions[q]))

            desc = combined[q].describe()
            print(".....")
            if "mean" in desc.index:
                print(desc)
            else:
                print("__N__ = {}".format(desc.loc["count"]))
            print()
            print(combined.groupby(q)[q].describe().iloc[:, 0:1])
            print(".....\n")

    # How many staff, by type of library?
    print(combined.groupby(["Q2", "Q4"])["Q4"].describe())

    # What kind of catalogue, by type of library?
    print(combined.groupby(["Q2", "Q11"])["Q11"].describe())


def one_way_anova(df, col1, col2):
    """
    Run a one-way ANOVA


    Based on https://www.statsmodels.org/devel/examples/notebooks/generated/interactions_anova.html
    """

    print("\n=== One-way ANOVA of {} by {}".format(col1, col2))
    c1 = col1
    c2 = col2

    if df[col1].dtypes == "object":
        c1 = "C({})".format(col1)
    if df[col2].dtypes == "object":
        c2 = "C({})".format(col2)
    lm = ols("{} ~ {}".format(c1, c2), data=df).fit()
    influence = lm.get_influence()
    table = anova_lm(lm)

    print(".....")
    print(lm.summary())
    print()
    print(influence.summary_table())
    print()
    print(table)
    print(".....\n")


def analyze_stats(combined, questions):
    "Find correlations with chi square"

    # Planned BIBFRAME transition time frame
    calc_chi_square(combined, questions, "Q2", "Q14")

    # Awareness of BIBFRAME, by type of library
    calc_chi_square(combined, questions, "Q2", "Q15")

    # Commitment - valence
    for valence in (
        "Q18_1",
        "Q18_2",
        "Q18_3",
        "Q18_4",
        "Q18_5",
        "Q18_6",
        "Q18_7",
        "Q18_8",
        "Q18_9",
        "Q18_valence",
    ):
        calc_chi_square(combined, questions, "Q2", valence, True)

    # Understanding by demographic
    for understanding in (
        "Q19",
        "Q21",
        "Q23_correct",
        "Q27_correct",
        "Q29_correct",
        "Q33_correct",
        "Q35_correct",
        "Q39_correct",
        "Q39_correct_strict",
        "Q41_correct",
        "Q43_correct",
    ):
        for col in ("Q2", "Q3", "Q4", "Q5", "Q7_English", "Q7_French", "Q8", "Q13"):
            calc_chi_square(combined, questions, col, understanding, True)

    # Understanding by confidence
    calc_chi_square(combined, questions, "Q20_1", "Q19", True)
    calc_chi_square(combined, questions, "Q22_1", "Q21", True)
    calc_chi_square(combined, questions, "Q24_1", "Q23_correct", True)
    calc_chi_square(combined, questions, "Q28_1", "Q27_correct", True)
    calc_chi_square(combined, questions, "Q30_1", "Q29_correct", True)
    calc_chi_square(combined, questions, "Q34_1", "Q33_correct", True)
    calc_chi_square(combined, questions, "Q36_1", "Q35_correct", True)
    calc_chi_square(combined, questions, "Q40_1", "Q39_correct", True)
    calc_chi_square(combined, questions, "Q40_1", "Q39_correct_strict", True)
    calc_chi_square(combined, questions, "Q42_1", "Q41_correct", True)
    calc_chi_square(combined, questions, "Q44_1", "Q43_correct", True)


if __name__ == "__main__":
    pd.set_option("display.max_columns", 1024)
    pd.set_option("display.max_rows", 100)

    # Load in the English dataframe
    eng = read_tsv("CBFR Survey - English - Final_January 16, 2020_13.30.tsv")
    # Load in the French dataframe
    fre = read_tsv("CBFR Survey - French - Final_January 16, 2020_13.28.tsv")

    questions = {
        "ResponseId": "ResponseId",
        "Q1": "Consent",
        "Q2": "Type of library",
        "Q3": "How many librarians?",
        "Q4": "How many staff?",
        "Q5": "Part of consortium?",
        "Q6": "Province or territory",
        "Q7": "Language of catalogue",
        "Q7_English": "Catalogue in English?",
        "Q7_French": "Catalogue in French?",
        "Q7_Cree": "Catalogue in Cree?",
        "Q7_Inuktitut": "Catalogue in Inuktitut?",
        "Q7_Inuinnaqtun": "Catalogue in Inuinnaqtun?",
        "Q7_Japanese": "Catalogue in Japanese?",
        "Q7_Mandarin": "Catalogue in Mandarin?",
        "Q7_Arabic": "Catalogue in Arabic?",
        "Q7_Farsi": "Catalogue in Farsi?",
        "Q7_Vietnamese": "Catalogue in Vietnamese?",
        "Q7_Spanish": "Catalogue in Spanish?",
        "Q8": "Primary responsibility",
        "Q8_10_TEXT": "Primary responsibility - Other",
        "Q8 Cataloguing": "Reponsibility includes cataloguing",
        "Q9": "Role includes training others",
        "Q10": "Role includes reportees",
        "Q11": "Catalogue system",
        "Q11_VENDOR": "Catalogue system vendor",
        "Q12": "Source of MARC records",
        "Q12_Copy": "Copy cataloguing from Z39.50 servers",
        "Q12_Original": "Original cataloguing",
        "Q12_Unsure": "Unsure",
        "Q12_Vendor": "Vendor",
        "Q12_OCLC": "OCLC",
        "Q12_Non-MARC": "My library does not currently use MARC records",
        "Q12_Sky River": "Sky River",
        "Q12_6_TEXT": "Source of MARC records - Other",
        "Q13": "Report holdings to OCLC",
        "Q14": "Planned BIBFRAME transition timeframe",
        "Q15": "BIBFRAME awareness prior to survey",
        "Q16_1": "Change commitment level",
        "Q16_2": "Change determination level",
        "Q16_3": "Change motivation level",
        "Q16_4": "Will do whatever it takes to implement",
        "Q16_5": "Want to implement this change",
        "Q16_commitment": "Commitment to implementing change",
        "Q17_1": "We can manage the politics of implementing this change",
        "Q17_2": "We can support people as they adjust to this change",
        "Q17_3": "We can coordinate tasks so implementation goes smoothly",
        "Q17_4": "We can keep track of progress in implementing this change",
        "Q17_5": "We can handle the challenges that might arise in implementing this change",
        "Q17_6": "We believe we have access to the training materials we need to implement this change",
        "Q17_efficacy": "Shared belief in capability to implement change",
        "Q18_1": "We feel this change is compatible with our values",
        "Q18_2": "We believe this change will benefit our community",
        "Q18_3": "We believe it is necessary to implement this change",
        "Q18_4": "We believe this change will work",
        "Q18_5": "We see this change as timely",
        "Q18_6": "We believe this change is cost-effective",
        "Q18_7": "We believe this change will make things better",
        "Q18_8": "We feel that implementing this change is a good idea",
        "Q18_9": "We value this change",
        "Q18_valence": "Conditions that determine commitment",
        "Q19": "True or false: every RDF triple consists of exactly one subject, one predicate, and one object.",
        "Q20_1": "Q19 confidence",
        "Q21": "True or false: each of the subjects, predicates, and objects of an RDF triple must be associated with a uniquely identifying link, called a Uniform Resource Identifier (URI).",
        "Q22_1": "Q21 confidence",
        "Q23": "Select each valid RDF serialization in the following list",
        "Q23 JSON-LD": "Q23 answer: JSON-LD",
        "Q23 N-Triples": "Q23 answer: N-Triples",
        "Q23 RDA": "Q23 answer: RDA",
        "Q23 RDF/XML": "Q23 answer: RDF/XML",
        "Q23 Turtle": "Q23 answer: Turtle",
        "Q23_correct": "Correct answer to Q23",
        "Q24_1": "Q23 confidence",
        "Q27": "What are the base classes that must appear in a description using the BIBFRAME vocabulary?",
        "Q27_correct": "What are the base classes that must appear in a description using the BIBFRAME vocabulary?",
        "Q28_1": "Q27 confidence",
        "Q29": "What set of BIBFRAME statements properly represents the Dewey Decimal call number of a book in your library collection?",
        "Q29_correct": "What set of BIBFRAME statements properly represents the Dewey Decimal call number of a book in your library collection?",
        "Q30_1": "Q29 confidence",
        "Q33": "When the subject heading is identified by a link, what is the best way to express a subject heading for a book using the BIBFRAME vocabulary?",
        "Q33_correct": "When the subject heading is identified by a link, what is the best way to express a subject heading for a book using the BIBFRAME vocabulary?",
        "Q34_1": "Q33 confidence",
        "Q35": "Select each of the vocabularies that can be used with BIBFRAME",
        "Q35_correct": "Select each of the vocabularies that can be used with BIBFRAME",
        "Q36_1": "Q35 confidence",
        "Q39": "Select all of the sources of links that BIBFRAME allows for use as identifiers in resource descriptions",
        "Q39_correct": "Select all of the sources of links that BIBFRAME allows for use as identifiers in resource descriptions (simplified)",
        "Q39_correct_strict": "Select all of the sources of links that BIBFRAME allows for use as identifiers in resource descriptions (strict)",
        "Q40_1": "Q39 confidence",
        "Q41": "What kind of interface will most cataloguers use to create a resource description in BIBFRAME?",
        "Q41_correct": "What kind of interface will most cataloguers use to create a resource description in BIBFRAME?",
        "Q42_1": "Q41 confidence",
        "Q43": "How will most people find library resources that have been described with BIBFRAME?",
        "Q43_correct": "How will most people find library resources that have been described with BIBFRAME?",
        "Q44_1": "Q43 confidence",
    }

    # Combine the two surveys into a single dataframe
    dataframe = eng.append(fre, ignore_index=True, sort=False)

    print("= Analyzing the CBRWG survey results")
    print(":toc:")
    dataframe = clean_responses(dataframe)
    dataframe = clean_columns(dataframe)
    dataframe = recode_values(dataframe)
    recode_role_other(dataframe)
    recode_system_type_other(dataframe)
    recode_library_type_other(dataframe)
    recode_marc_source(dataframe)
    calc_oric(dataframe)
    create_Q23_subvariables(dataframe)
    create_correct(dataframe, "Q27", "Work, Instance, and Item")
    create_correct(dataframe, "Q29", "IM_3JypoEBKmHPYmoJ")
    create_correct(dataframe, "Q33", "IM_3n1dkDeX3nr3q9T")
    create_correct(
        dataframe,
        "Q35",
        "Bibliographic Ontology \(BIBO\),Dublin Core Metadata Initiative \(DCMI\),Schema.org,Resource Description & Access \(RDA\),FRBR-aligned Bibliographic Ontology \(FaBiO\)",
    )
    create_correct(dataframe, "Q39", "Any web domain")

    col = "Q39"
    df = dataframe
    correct = "Any web domain"
    correct_col = "{}_correct_strict".format(col)
    df.loc[(df[col].any()) & (df[col].str.match(r".")), correct_col] = "No"
    df.loc[
        (df[col].any()) & (df[col].str.match("^{}$".format(correct))), correct_col
    ] = "Yes"
    df.loc[
        (df[col].any()) & (df[col].str.contains("I don't know")), correct_col
    ] = "I don't know"

    create_correct(dataframe, "Q41", "typeahead search")
    create_correct(dataframe, "Q43", "users will search for a term or phrase")
    print("== Describing data\n")
    describe_stats(dataframe, questions)
    print("\n== Analyzing data")
    analyze_stats(dataframe, questions)
    one_way_anova(dataframe, "Q16_commitment", "Q2")
    one_way_anova(dataframe, "Q17_efficacy", "Q2")
    dataframe = anonymize(dataframe)
    write_results(dataframe, questions, "combined_results.tsv")
